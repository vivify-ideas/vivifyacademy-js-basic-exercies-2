/*

1.  U JavaScript-u napraviti promenljivu unorderedList i u nju smestiti HTML element <ul> sa ID-em list. Ovaj element se može dobiti pomoću funkcije document.getElementById (https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById).
2.  U JavaScript-u logovati u konzoli prvi child element unutar unorderedList. Child elementi su elementi unutar nekog drugog, u ovom slucaju <li> elementi su child-ovi <ul> elementa. U JavaScript-u se child elementi dobijaju preko property-a elementa children (https://developer.mozilla.org/en-US/docs/Web/API/ParentNode/children).
3.  Proći kroz sve child-ove elementa unorderedList i logovati ih u konzoli. unorderedList.children objekat ima length, i može se proći kroz njega for petljom.
4.  Izmeniti tekst prvog <li> elementa u "First item (edited)". Ovo se može postići dodeljivanjem vrednosti property-u innerHTML (https://developer.mozilla.org/en-US/docs/Web/API/Element/innerHTML).
5.  Pomoću funkcije document.createElement napraviti novi <li> element, postaviti mu innerHTML na neki tekst po vašem izboru i logovati taj element u konzoli (https://developer.mozilla.org/en-US/docs/Web/API/Document/createElement).
6.  Dodati event listener na <button> element, koji će na klik dugmeta ispisati u konzoli "Button clicked!" (https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener).
7.  Ukucati neki tekst u <input> element, zatim na klik dugmeta ispisati u konzoli tekst koji je unešen (dobija se preko value property-a).
8.  Na klik <button> elementa napraviti novi <li> element, kao innerHTML mu postaviti trenutnu vrednost <input> elementa, i dodati ga kao child <ul> elementa pomoću appendChild (https://developer.mozilla.org/en-US/docs/Web/API/Node/appendChild).
9.  Izmeniti kod iz prethodnog zadatka tako da se nakon dodavanja novog <li> u <ul> brise vrednost <input> elementa.
10. Izmeniti kod iz prethodnog zadatka tako da se na klik dugmeta ono isključi na 3 sekunde (postavi mu se disabled property na true, zatim se posle 3 sekunde vrati na false).
11. Postaviti na <button> event listener koji radi sledeće: na klik treba da se obriše <input>, <button> se disable-uje i promeni mu se tekst na "Working...", i nakon 3 sekunde se <button> ponovo uključi, tekst mu se vrati na "Add new item", a u <ul> se ubaci novi <li> sa tekstom koji je bio u <input> elementu pre brisanja.

*/


// 1.
var unorderedList = document.getElementById('list');


// 2.
console.log(unorderedList.children[0]);


// 3.
// Sa for petljom:
for (var i = 0; i < unorderedList.children.length; i++) {
    console.log(unorderedList.children[i]);
}
// Sa for-of petljom:
for (var child of unorderedList.children) {
    console.log(child);
}


// 4.
unorderedList.children[0].innerHTML = 'First item (edited)';


// 5.
var newLi = document.createElement('li');
newLi.innerHTML = 'Third item';
console.log(newLi);


// 6.
var button = document.getElementById('add-new');
button.addEventListener('click', function() {
    console.log('Button clicked!');
});


// 7.
var input = document.querySelector('input');
button.addEventListener('click', function() {
    console.log(input.value);
});


// 8.
function addNewItem8() {
    var newLi = document.createElement('li');
    newLi.innerHTML = input.value;
    unorderedList.appendChild(newLi);
}
button.addEventListener('click', addNewItem8);


// 9.
button.removeEventListener('click', addNewItem8);    // sklanja se stari da ne bi dodavao dvaput
function addNewItem9() {
    var newLi = document.createElement('li');
    newLi.innerHTML = input.value;
    input.value = '';
    unorderedList.appendChild(newLi);
}
button.addEventListener('click', addNewItem9);


// 10.
button.removeEventListener('click', addNewItem9);
function addNewItem10() {
    var newLi = document.createElement('li');
    newLi.innerHTML = input.value;
    input.value = '';
    unorderedList.appendChild(newLi);

    button.disabled = true;
    setTimeout(function() {
        button.disabled = false;
    }, 3000);
}
button.addEventListener('click', addNewItem10);


// 11.
button.removeEventListener('click', addNewItem10);
function addNewItem11() {
    var savedInputValue = input.value;
    input.value = '';

    button.disabled = true;
    button.innerHTML = 'Working...';

    setTimeout(function() {
        var newLi = document.createElement('li');
        newLi.innerHTML = savedInputValue;
        unorderedList.appendChild(newLi);

        button.disabled = false;
        button.innerHTML = 'Add new item';
    }, 3000);
}
button.addEventListener('click', addNewItem11);
